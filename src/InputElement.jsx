import React, { useState } from "react";

const InputElement = (props) => {
  const [Input, setInput] = useState("");
  const [HistoryList, setHistoryList] = useState([]);

  return (
    <>
      <input
        value={Input}
        onChange={(event) => {
          setInput(event.target.value);
          setHistoryList([...HistoryList, event.target.value]);
        }}
      />
      <div>{Input}</div>
      <div>
        {HistoryList.map((x) => (
          <div>{x}</div>
        ))}
      </div>
    </>
  );
};

export default InputElement;

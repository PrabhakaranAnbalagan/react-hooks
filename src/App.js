import React from "react";
import "./App.css";
import  InputElement  from "./InputElement";

function App() {
  return (
    <div className="App">
      <InputElement />
    </div>
  );
}

export default App;
